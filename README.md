FlashlightPlus [![Build Status](https://travis-ci.org/FallenYouth/FlashlightPlus.svg?branch=master)](https://travis-ci.org/FallenYouth/FlashlightPlus)
==============

FlashlightPlus is a flashlight plugin for minecraft that applies a potion effect that allows players to see in dark caves.

###Supported Platforms:
* Bukkit
* Spigot

License
==============
FlashlightPlus is licensed under the [MIT License](http://opensource.org/licenses/MIT). Feel free to contribute to the project.
